BASENAME = paintDeform
APPTYPE  = xsi

OBJS += paintDeform
OBJS += brushes

LIBS += sicppsdk
LIBS += sicoresdk
LIBS += GL
LIBS += GLU

#AL_MAKELIB_USE_BOOST = 1

ifeq ($(MODE), debug)
	CPPFLAGS += -ggdb
endif

include $(AL_MAKELIB)/Makefile.common

