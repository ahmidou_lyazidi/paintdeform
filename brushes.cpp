#include "brushes.h"

brushes::brushes()
{
	outRadius  = 2.0f;
	initOutRadius =  2.0f;
	inRadius = 1.0f;
	opacity = 0.05f;
	initOpacity = opacity;
	invert = 1;

}

void brushes::push( ToolContext &in_ctxt, CLongArray& indices, CFloatArray& lDist, CVector3Array& posArray, CVector3Array& normArray)
{
	for(LONG i = 0; i < indices.GetCount(); i++)
	{
		CVector3 push;
		normArray[indices[i]].ScaleInPlace(1-(lDist[i]/outRadius)).ScaleInPlace(opacity);
		push.Add(posArray[indices[i]],normArray[indices[i]]);
		posArray[indices[i]].Set(push.GetX(),push.GetY(),push.GetZ());
	}
}

void brushes::pinch( ToolContext &in_ctxt, CLongArray& indices, CFloatArray& lDist, CVector3Array& posArray)
{
	for(LONG i = 0; i < indices.GetCount(); i++)
	{
		//Application().LogMessage(CString(indices.GetCount()));
		CVector3 push;
		posArray[indices[i]].Set(push.GetX(),push.GetY(),push.GetZ());
	}
}

void brushes::smooth( ToolContext &in_ctxt, PolygonMesh &oGeo2, CLongArray& indices, CFloatArray& lDist, CVector3Array& posArray)
{
	CVertexRefArray vertices(oGeo2.GetVertices());
	for(LONG i = 0; i < indices.GetCount(); i++)
	{
		Vertex vertex(vertices.GetItem(indices[i]));
		CVertexRefArray neighbors(vertex.GetNeighborVertices(1));
		//Application().LogMessage(CString(neighbors.GetCount()));
		CVector3 smooth(posArray[indices[i]]);
		CVector3 aver(0,0,0);
		for(LONG j = 0; j < neighbors.GetCount(); j++)
		{
			Vertex oNeighbors = neighbors.GetItem(j);
			int oNidx = oNeighbors.GetIndex();
			//Application().LogMessage(L" IDX = "+CString(oNidx));
			CVector3 nPos(posArray[oNidx]);
			nPos.SubInPlace(smooth).ScaleInPlace(0.5);
			//Application().LogMessage(L"adj point "+CString(oNidx)+" position is: "+CString(nPos.GetX())+" Y="+CString(nPos.GetY())+" Z="+CString(nPos.GetZ()));
			aver.AddInPlace(nPos);
		}
		float factor =1.0f/float(neighbors.GetCount());
		aver.ScaleInPlace(factor).ScaleInPlace(opacity);
		smooth.AddInPlace(aver);
		//app.LogMessage(L"smoothX "+CString(smooth.GetX())+" Y="+CString(smooth.GetY())+" Z="+CString(smooth.GetZ()));

		posArray[indices[i]].Set(smooth.GetX(),smooth.GetY(),smooth.GetZ());
	}
}
