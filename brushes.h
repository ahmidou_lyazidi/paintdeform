#ifndef BRUSHES_H
#define BRUSHES_H
///////////////////////////////////////////////////////////////////////////
//
// File: 3DManipulator.h
//
// Description: 3DManipulator class
//

///////////////////////////////////////////////////////////////////////////
#include <xsi_toolcontext.h>

#ifndef linux
#define WIN32_LEAN_AND_MEAN
#include <windows.h> // Needed for OpenGL on windows
#endif
#include <string.h>
#include <xsi_application.h>
#include "paintDeform.h"

using namespace XSI;
using namespace XSI::MATH;

////////////////////////////////////////////////////////////////////////////////
//
// 3DManipulator class
//
////////////////////////////////////////////////////////////////////////////////
//Every class that has a pointer data member should include the following member functions:
//   a destructor,
//   a copy constructor,
//   operator= (assignment) 
class brushes {
public:
	brushes();
	void push(ToolContext &in_ctxt, CLongArray& indices, CFloatArray& lDist, CVector3Array& posArray, CVector3Array& normArray);
	void pinch(ToolContext &in_ctxt, CLongArray& indices, CFloatArray& lDist, CVector3Array& posArray);
	void smooth(ToolContext &in_ctxt, PolygonMesh &oGeo2, CLongArray& indices, CFloatArray& lDist, CVector3Array& posArray);	

	float initOutRadius ;
	float outRadius ;
	float inRadius;
	float initOpacity;
	float opacity;
	bool invert;

private: // Data
	
};

#endif // AL_3DMANIPULATOR_H
