#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#ifndef l_timer
	 #include "l_time.h"
	 #define l_timer
#endif

void CStopWatch::startTimer( ) {
	gettimeofday(&start, NULL);
}

void CStopWatch::stopTimer( ) {
	gettimeofday(&end, NULL);
}

double CStopWatch::getElapsedTime() {
	long int diff = (end->tv_usec + 1000000 * end->tv_sec) - (start->tv_usec + 1000000 * start->tv_sec);
	result->tv_sec = diff / 1000000;
	result->tv_usec = diff % 1000000;
	return (diff<0);
	printf("C %ld.%06ld\n", diff.tv_sec, diff.tv_usec);
}
