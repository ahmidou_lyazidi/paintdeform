#ifndef L_TIME_H
#define L_TIME_H

#include <sys/time.h>
 
class CStopWatch {

private:
	struct timeval start, end, diff;

public:
	struct timeval result;
	void startTimer( ) ;
	void stopTimer( ) ;
	double getElapsedTime() ;
};

#endif
