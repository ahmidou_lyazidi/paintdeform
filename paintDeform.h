#ifndef BRUSHTOOL_H
#define BRUSHTOOL_H

#include <algorithm>
#include <xsi_math.h>
#include <xsi_context.h>
#include <xsi_utils.h>
#include <xsi_argument.h>
#include <xsi_command.h>
#include <xsi_toolcontext.h>
#include <xsi_menu.h>
#include <xsi_menuitem.h>
#include <xsi_selection.h>
#include <xsi_primitive.h>
#include <xsi_polygonmesh.h>
#include <xsi_polygonface.h>
#include <xsi_comapihandler.h>
#include <xsi_kinematics.h>
#include <xsi_matrix4.h>
#include <xsi_rotation.h>
#include <xsi_edge.h>
#include <xsi_facet.h>
#include <xsi_vertex.h>
#include <xsi_operator.h>
#include <xsi_parameter.h>
#include <xsi_preferences.h>
#include <xsi_filter.h>
#include <xsi_plugin.h>
#include <xsi_geometryaccessor.h>

#endif
